const red = [255,128,128];
const yellow = [255,255,128];
const green = [128,255,128];
const colorScale = [red, yellow, green];

function ratioColor(n) {
    const numColors = colorScale.length;
    const numScales = numColors - 1;
    const scaledN = numScales * n;
    const scaleNum = Math.max(0,Math.min(Math.ceil(scaledN)-1, numScales-1));
    const colorLo = colorScale[scaleNum];
    const colorHi = colorScale[scaleNum+1];
    const scalePosition = scaledN - scaleNum;
    const [r, g, b] = [0,1,2].map((colorPosition) =>
        Math.floor((1-scalePosition) * colorLo[colorPosition] + scalePosition * colorHi[colorPosition])
    );
    return `rgb(${r},${g},${b})`;
};
// label = (ratio*100).toFixed(0);

// const n = Number(process.argv[2]);
const got = Number(process.argv[2]);
const of = Number(process.argv[3]);
const n = got/of;

console.log(ratioColor(n));
