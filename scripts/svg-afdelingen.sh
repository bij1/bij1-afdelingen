#!/usr/bin/bash
sed -i "s~</style>~	.Groningen{fill:#9AF3E1;}\\
	.Groningen>path{fill:#9AF3E1;}\\
	.Friesland{fill:#E5CAFF;}\\
	.Friesland>path{fill:#E5CAFF;}\\
	.Almere{fill:#ACF6A9;}\\
    .Arnhem_Nijmegen{fill:#F5C6B6;}\\
    .Eindhoven{fill:#FFC0C6;}\\
    .Tilburg{fill:#ABFD81;}\\
    .Breda{fill:#DFDFDF;}\\
    .Zuid_Limburg{fill:#DFDFDF;}\\
    .Den_Bosch{fill:#DFDFDF;}\\
    .Amersfoort{fill:#97FFF6;}\\
    .Utrecht{fill:#DDAEF6;}\\
    .Hilversum{fill:#FEC1A4;}\\
    .Amsterdam{fill:#FCB3F8;}\\
    .Zaanstreek{fill:#FFCCCB;}\\
    .Haarlem{fill:#D8B3F6;}\\
    .Leiden{fill:#83FF84;}\\
    .Den_Haag{fill:#7EFBED;}\\
    .Delft{fill:#3CCFF1;}\\
    .Rotterdam{fill:#FDDC8B;}\\
    .Rotterdam>path{fill:#FDDC8B;}\\
</style>~" ./generated/gemeenten-afdelingen.svg

csv2json ./out/afdelingsgebieden.csv | jq -c '.[]' | while read -r gemeente; do \
    code=$(echo $gemeente | jq '.GemeentecodeGM'); \
    class=$(echo $gemeente | jq '.afdeling' | sed -n "s/[\"']//pg;s/\s/_/pg;s/-/_/pg"); \
    perl -0777 -i -pe "s~(id=${code} class=\"st0)\"~\$1 ${class}\"~is" ./generated/gemeenten-afdelingen.svg; \
    perl -0777 -i -pe "s~(id=${code})>~\$1 class=\"${class}\">~is" ./generated/gemeenten-afdelingen.svg; \
done
