#!/usr/bin/bash
csv2json ./out/afdelingsgebieden.csv | jq -c '.[]' | while read -r gemeente; do \
    code=$(echo $gemeente | jq '.GemeentecodeGM'); \
    # echo "code: ${code}"
    got=$(echo $gemeente | jq '.verzamelde_ondersteuningsverklaringen' | sed -n 's/"//pg')
    # echo "got: ${got}"
    of=$(echo $gemeente | jq '.benodigde_ondersteuningsverklaringen' | sed -n 's/"//pg')
    # echo "of: ${of}"
    if [[ "$of" != '' ]]; then
        echo "((${got}. / ${of}))"
        # progress=$((${got}. / ${of}))
        # echo "progress: ${progress}"
        # color=$(node ./scripts/ratio_color.js $progress)
        color=$(node ./scripts/ratio_color.js $got $of)
        # echo "color: ${color}"
        perl -0777 -i -pe "s~(id=${code})~\$1 style=\"fill:${color};\"~is" ./generated/gemeenten-ondersteuningsverklaringen.svg; \
        perl -0777 -i -pe "s~(id=${code}[^/]*?)</title>~\$1: $got / $of</title>~is" ./generated/gemeenten-ondersteuningsverklaringen.svg; \
    fi
done
