# convert to utf8, for both sqlite and git
iconv -f iso-8859-1 -t utf8 ./raw/pc7/gem2020.csv -o ./raw/pc7/gemeenten2020.csv
# deal with mismatching municipalities between 2020 and 2021
# map municipalities to their present forms, where possible
cp ./raw/pc7/gemeenten2020.csv ./out/gemeenten2020_2021.csv
# the Haaren replacement is a simplification, given this municipality was split:
# Haaren -> Boxtel / Oisterwijk / Vught / Tilburg
sed -i 's/Appingedam/Eemsdelta/g;s/Delfzijl/Eemsdelta/g;s/Loppersum/Eemsdelta/g;s/Haaren/Tilburg/g' ./out/gemeenten2020_2021.csv
