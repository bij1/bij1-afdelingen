#!/usr/bin/bash
csv2json ./raw/gemeenten.csv | jq -c '.[]' | while read -r gemeente; do \
    code=$(echo $gemeente | jq '.GemeentecodeGM'); \
    title="$(echo $gemeente | jq '.Gemeentenaam' | sed -n 's/"//pg') ($(echo $gemeente | jq '.Provincienaam' | sed -n 's/"//pg'))"; \
    perl -0777 -i -pe "s~(<(path|polygon) id=${code} [^/]+)/>~\$1><title>${title}</title></\$2>~is" ./generated/gemeenten-tooltipped.svg; \
    perl -0777 -i -pe "s~(<g id=${code}>)\$~\$1<title>${title}</title>~is" ./generated/gemeenten-tooltipped.svg; \
done
