# BIJ1 Afdelingen

Data and maps on BIJ1's [local branches](https://gitlab.com/bij1/bij1-afdelingen/-/blob/main/source/afdelingen.csv) in the Netherlands, and their [various areas](https://gitlab.com/bij1/bij1-afdelingen/-/blob/main/source/bij1-gebieden.csv).

## goals

- [x] Formalize data on BIJ1 branches and their areas:
  - municipality
  - BIJ1 branch
- [ ] incorporate Caribbean municipalities
- [x] incorporate branches in founding
- [ ] add branch status (in founding, official, inactive)
- [ ] Figure out the above given a member's address
- Make SVGs of the above areas, with tooltips:
  - [x] branch areas
  - [x] ondersteuningsverklaringen
- [x] Facilitate automatically updating the above,
  including when source data changes.

## caveats

- Our best data mapping addresses to municipality
  is based on a 2020 dataset consisting of postal code
  (in pc6 format, e.g. 1234AB) plus number (huisnummer).
  Such a mapping is not fully deterministic however,
  as this would require also the extensions (e.g. 1A),
  data on which seems nonexistant.
- Having used that 2020 dataset, we had to guess
  a present municipality for the former municipality of Haaren,
  which in 2021 was split into Boxtel / Oisterwijk / Vught / Tilburg.

## requirements

- a Unix environment (Linux, Mac, WSL, ...)
- [Make](https://www.gnu.org/software/make/)
- [SQLite 3](https://sqlite.org/index.html)
- `dos2unix`
- `jq`
- [NodeJS](https://nodejs.org/en/)
  - package [csv2json](https://www.npmjs.com/package/csv2json)

## usage

```bash
make run_all
```

## branch area maps

### designed

#### branch areas

![branch areas](./design/kaartje-BIJ1-gemeentes+regio's.jpg)

### generated

#### branch areas

![branch areas](./generated/gemeenten-afdelingen.svg)
